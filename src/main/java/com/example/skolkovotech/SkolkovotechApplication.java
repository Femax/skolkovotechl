package com.example.skolkovotech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SkolkovotechApplication {
    public static void main(String args[]) {
        SpringApplication.run(SkolkovotechApplication.class, args);
    }
}
