package com.example.skolkovotech;

import com.example.skolkovotech.models.request.SensorEventRequest;
import com.example.skolkovotech.models.response.BuildingResponse;
import com.example.skolkovotech.models.response.SensorAverageResponse;
import com.example.skolkovotech.models.response.SensorEventResponse;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class JdbcService {

    private JdbcTemplate jdbcTemplate;

    public int saveSensorEvent(int sensorId, SensorEventRequest sensorEvent) {
        return jdbcTemplate.update("insert\n" +
                        "into sensor_event\n" +
                        "values (?, ?, ?);",
                sensorEvent.getDate(), sensorId, sensorEvent.getValue());
    }

    public List<BuildingResponse> getLastSensorInBuilding(int buildingId) {
        return jdbcTemplate.query(
                "select s.id,\n" +
                        "       s.name,\n" +
                        "       se.sensor_id,\n" +
                        "       se.date_time,\n" +
                        "       se.value\n" +
                        "from sensor s\n" +
                        "         inner join sensor_event se\n" +
                        "         inner join (SELECT sensor_id, max(sensor_event.date_time) as date_time\n" +
                        "                     from sensor_event\n" +
                        "                     GROUP BY sensor_id) sem\n" +
                        "                    on se.sensor_id = sem.sensor_id and se.date_time = sem.date_time\n" +
                        "                    on sem.sensor_id = s.id\n" +
                        "where s.building_id = ?;"
                ,
                new Object[]{buildingId},
                (rs, rowNum) -> BuildingResponse
                        .builder()
                        .buildingId(buildingId)
                        .sensorEvent(SensorEventResponse
                                .builder()
                                .id(rs.getInt("id"))
                                .date(rs.getTimestamp("date_time"))
                                .value(rs.getDouble("value"))
                                .sensorId(rs.getInt("sensor_id"))
                                .build())
                        .build());
    }

    public List<SensorAverageResponse> getAverageValueOfSensor() {
        return jdbcTemplate.query("select s.id,\n" +
                        "       s.name,\n" +
                        "       se.sensor_id,\n" +
                        "       se.average_value\n" +
                        "from sensor s\n" +
                        "         inner join (SELECT sensor_id, avg(sensor_event.value) as average_value\n" +
                        "                     from sensor_event\n" +
                        "                     GROUP BY sensor_id) se\n" +
                        "                    on se.sensor_id = s.id",
                (rs, rowNum) -> SensorAverageResponse
                        .builder()
                        .averageValue(rs.getDouble("average_value"))
                        .sensorId(rs.getInt("id"))
                        .build());
    }

    public List<SensorEventResponse> getSensorEventsBySensorBetweenDate(int sensorId, Date from, Date to) {
        return jdbcTemplate.query("select *\n" +
                        "from sensor_event\n" +
                        "where sensor_event.sensor_id = ? \n" +
                        "  and sensor_event.date_time between ? and ?",
                new Object[]{sensorId, from, to},
                (rs, rowNum) -> SensorEventResponse
                        .builder()
                        .value(rs.getDouble("value"))
                        .sensorId(rs.getInt("sensor_id"))
                        .id(rs.getLong("id"))
                        .date(rs.getTimestamp("date_time"))
                        .build());
    }
}
