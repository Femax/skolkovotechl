package com.example.skolkovotech.models.request;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class SensorEventRequest {
    private int sensorId;
    private Date date;
    private Double value;
}
