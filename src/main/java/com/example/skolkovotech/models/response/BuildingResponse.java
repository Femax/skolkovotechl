package com.example.skolkovotech.models.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BuildingResponse {
    private long buildingId;
    private SensorEventResponse sensorEvent;
}
