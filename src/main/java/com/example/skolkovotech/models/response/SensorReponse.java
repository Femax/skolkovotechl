package com.example.skolkovotech.models.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SensorReponse {
    private long id;
    private String name;
    private double value;
}
