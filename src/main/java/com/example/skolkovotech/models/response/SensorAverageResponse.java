package com.example.skolkovotech.models.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SensorAverageResponse {
    private int sensorId;
    private double averageValue;
}
