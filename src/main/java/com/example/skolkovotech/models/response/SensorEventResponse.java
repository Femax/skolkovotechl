package com.example.skolkovotech.models.response;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class SensorEventResponse {
    private long id;
    private int sensorId;
    private Date date;
    private Double value;
}
