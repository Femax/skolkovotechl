package com.example.skolkovotech;

import com.example.skolkovotech.models.request.SensorEventRequest;
import com.example.skolkovotech.models.response.BuildingResponse;
import com.example.skolkovotech.models.response.SensorAverageResponse;
import com.example.skolkovotech.models.response.SensorEventResponse;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@AllArgsConstructor
public class Controller {

    private JdbcService jdbcService;

    @GetMapping(path = "sensor", params = {"startDate", "finishDate", "id"})
    public ResponseEntity<List<SensorEventResponse>> getSensorByDateAndName(@RequestParam(value = "startDate")
                                                                            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                                                                    Date startDate,
                                                                            @RequestParam(value = "finishDate")
                                                                            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                                                                    Date finishDate,
                                                                            @RequestParam(value = "id", required = false)
                                                                                    int id) {
        return ResponseEntity.ok(jdbcService.getSensorEventsBySensorBetweenDate(id, startDate, finishDate));
    }

    @GetMapping(path = "averageSensor")
    public List<SensorAverageResponse> getAllAverageSensor() {
        return jdbcService.getAverageValueOfSensor();
    }

    @GetMapping(path = "building/{id}/lastSensorEvents")
    public List<BuildingResponse> getLastsValuesOfSensorByBuilding(@PathVariable int id) {
        return jdbcService.getLastSensorInBuilding(id);
    }

    @PostMapping("sensor/{id}/sensorEvent")
    public boolean createSensor(@PathVariable int id, @RequestBody SensorEventRequest sensorEventDto) throws Exception {
        return jdbcService.saveSensorEvent(id, sensorEventDto) > 0;
    }

}
