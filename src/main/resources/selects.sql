-- select last sensors
select s.id,
       s.name,
       se.sensor_id,
       se.date_time,
       se.value
from sensor s
         inner join sensor_event se
         inner join (SELECT sensor_id, max(sensor_event.date_time) as date_time
                     from sensor_event
                     GROUP BY sensor_id) sem
                    on se.sensor_id = sem.sensor_id and se.date_time = sem.date_time
                    on sem.sensor_id = s.id
where s.building_id = 1;
-- getAverage sensor
select s.id,
       s.name,
       se.sensor_id,
       se.value
from sensor s
         inner join (SELECT sensor_id, avg(sensor_event.value) as value
                     from sensor_event
                     GROUP BY sensor_id) se
                    on se.sensor_id = s.id;
-- get  sensors_event by sensor_id
select *
from sensor_event
where sensor_event.sensor_id = 1
  and sensor_event.date_time between '2019-08-29 16:42:44.286000' and '2019-08-30 16:42:32.640000';

explain analyse select s.id,
                       s.name,
                       se.sensor_id,
                       se.date_time,
                       se.value
                from sensor s
                         inner join sensor_event se
                         inner join (SELECT sensor_id, max(sensor_event.date_time) as date_time
                                     from sensor_event
                                     GROUP BY sensor_id) sem
                                    on se.sensor_id = sem.sensor_id and se.date_time = sem.date_time
                                    on sem.sensor_id = s.id
                where s.building_id = 1;
